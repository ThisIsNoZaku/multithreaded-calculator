package com.thinoza.calculator;

import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

public class CalculatorEngine {
	private ForkJoinPool executor = new ForkJoinPool();

	public BigDecimal parse(String expression) throws ExecutionException {
		try {
			return executor.submit(new Expression(expression)).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return null;
	}
}