package com.thinoza.calculator;

import java.lang.reflect.Executable;
import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.regex.Pattern;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import javax.swing.Action;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.plaf.basic.BasicInternalFrameTitlePane.RestoreAction;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;

public class Calculator extends JPanel {
	private final ActionListener numberButtonAction = new NumberButtonAction();
	private final ActionListener operatorButtonAction = new OperatorButtonAction();
	private boolean overwrite = true;
	private final CalculatorEngine engine = new CalculatorEngine();

	public Calculator() {
		setLayout(new MigLayout("", "[][][][][][]", "[][][][][]"));

		displayField = new JTextField("0");
		add(displayField, "cell 0 0 5 1,growx");
		displayField.setColumns(10);

		JButton btnDivide = new JButton("/");
		add(btnDivide, "cell 4 2");
		btnDivide.addActionListener(operatorButtonAction);

		JButton btn_1 = new JButton("1");
		add(btn_1, "cell 0 3");
		btn_1.addActionListener(numberButtonAction);

		JButton btn_2 = new JButton("2");
		add(btn_2, "cell 1 3");
		btn_2.addActionListener(numberButtonAction);

		JButton btn_3 = new JButton("3");
		add(btn_3, "cell 2 3");
		btn_3.addActionListener(numberButtonAction);

		JButton btn_4 = new JButton("4");
		add(btn_4, "cell 0 2");
		btn_4.addActionListener(numberButtonAction);

		JButton btn_5 = new JButton("5");
		add(btn_5, "cell 1 2");
		btn_5.addActionListener(numberButtonAction);

		JButton btn_6 = new JButton("6");
		add(btn_6, "cell 2 2");
		btn_6.addActionListener(numberButtonAction);

		JButton btn_7 = new JButton("7");
		add(btn_7, "cell 0 1");
		btn_7.addActionListener(numberButtonAction);

		JButton btn_8 = new JButton("8");
		add(btn_8, "cell 1 1");
		btn_8.addActionListener(numberButtonAction);

		JButton btn_9 = new JButton("9");
		add(btn_9, "cell 2 1");
		btn_9.addActionListener(numberButtonAction);

		JButton btnLeftParens = new JButton("(");
		add(btnLeftParens, "cell 3 1");
		btnLeftParens.addActionListener(operatorButtonAction);

		JButton btnRightParens = new JButton(")");
		add(btnRightParens, "cell 4 1");
		btnRightParens.addActionListener(operatorButtonAction);

		JButton btnMultiply = new JButton("*");
		add(btnMultiply, "cell 3 2");
		btnMultiply.addActionListener(operatorButtonAction);

		JButton btnEquals = new JButton("=");
		btnEquals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String result = null;
				try {
					result = engine.parse(displayField.getText()) != null ? engine.parse(displayField.getText()).toString(): "Err";
				} catch (ExecutionException e1) {
					e1.printStackTrace();
				}
				displayField.setText(result);
				overwrite = true;
			}
		});

		JButton btnAdd = new JButton("+");
		add(btnAdd, "cell 3 3");
		btnAdd.addActionListener(operatorButtonAction);

		JButton btnSubtract = new JButton("-");
		add(btnSubtract, "cell 4 3");
		btnSubtract.addActionListener(numberButtonAction);
		add(btnEquals, "cell 3 4 2 1");
	}

	private JTextField displayField;

	private class NumberButtonAction implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (overwrite == true) {
				Calculator.this.displayField.setText(((JButton) e.getSource()).getText());
				overwrite = false;
			} else {
				Calculator.this.displayField.setText(displayField.getText()
						+ ((JButton) e.getSource()).getText());
			}
		}
	}

	private class OperatorButtonAction implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			Calculator.this.displayField.setText(displayField.getText()
					+ ((JButton) e.getSource()).getText());
			overwrite = false;
		}
	}

	public static final void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new Calculator());
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
}