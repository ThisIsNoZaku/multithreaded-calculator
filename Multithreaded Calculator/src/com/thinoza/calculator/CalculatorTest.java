package com.thinoza.calculator;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.concurrent.ExecutionException;

import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {
    CalculatorEngine c = new CalculatorEngine();

    /**
     * Test that a a simple number returns itself.
     */
    @Test
    public void testValueParsing() throws ExecutionException {
        assertTrue(c.parse("1").equals(new BigDecimal("1")));
    }

    /**
     * Test simple addition.
     */
    @Test
    public void testSimpleAdding() throws ExecutionException {
        assertTrue(c.parse("1+1").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test simple subtraction.
     */
    @Test
    public void testSimpleSubtration() throws ExecutionException {
        assertTrue(c.parse("3-1").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test simple multiplication.
     */
    @Test
    public void testSimpleMultiplication() throws ExecutionException {
        assertTrue(c.parse("2*1").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test simple division
     */
    @Test
    public void testSimpleDivision() throws ExecutionException {
        assertTrue(c.parse("4/2").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test that operator precedence is adhered to.
     */
    @Test
    public void testOperatorPrecedence() throws ExecutionException {
        assertTrue(c.parse("2+4/2").equals(new BigDecimal(Integer.parseInt("4"))));
        assertTrue(c.parse("4/2+2").equals(new BigDecimal(Integer.parseInt("4"))));
        assertTrue(c.parse("2*2+2*2+2-2*2").equals(new BigDecimal(Integer.parseInt("6"))));
        assertTrue(c.parse("2+2*2-2/2").equals(new BigDecimal(Integer.parseInt("5"))));
    }

    /**
     * Test that parentheses are handled correctly
     */
    @Test
    public void testParentheses() throws ExecutionException {
        assertTrue(c.parse("(2+4)/2").equals(new BigDecimal(Integer.parseInt("3"))));
        assertTrue(c.parse("2+(4/2)").equals(new BigDecimal(Integer.parseInt("4"))));
    }

    /**
     * Test that multiple nested parentheses are handled correctly
     */
    @Test
    public void testNestedParentheses() throws ExecutionException {
        assertTrue(c.parse("((2+4)/(1+1))*2").equals(new BigDecimal(Integer.parseInt("6"))));
    }

    /**
     * Test lots of nested parentheses.
     */
    @Test
    public void testMultipleNestedParentheses() throws ExecutionException {
        assertTrue(c.parse("(((((1+1)))))").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test an invalid expression where multiple operators are placed
     * next to each other.
     */
    @Test(expected = NumberFormatException.class)
    public void testAdjacentOperators() throws Throwable {
        try {
            assertTrue(c.parse("1++1").equals(new BigDecimal(Integer.parseInt("2"))));
        }catch (ExecutionException ex){
            throw ex.getCause();
        }
    }

    /**
     * Test unmatched right parenthesis.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testUnclosedLeftParentheses() {
        try {
            assertTrue(c.parse("1+1)").equals(new BigDecimal(Integer.parseInt("2"))));
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
            throw e;
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test unmatched parenthesis.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testUnclosedParentheses() throws ExecutionException {
        assertTrue(c.parse("(1+1").equals(new BigDecimal(Integer.parseInt("2"))));
    }

    /**
     * Test factorial
     */
    @Test
    public void testFactorial() throws ExecutionException {
        BigDecimal expected = new BigDecimal("120");
        BigDecimal actual = c.parse("!5");
        assertTrue(String.format("Expected %s but was %s", expected.toString(), actual.toString()), expected.equals(actual));
    }
}