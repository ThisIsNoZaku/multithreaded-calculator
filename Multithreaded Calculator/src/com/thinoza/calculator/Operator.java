package com.thinoza.calculator;

enum Operator {
	Plus("+", 0), Subtract("-", 0), Multipy("*", 10), Divide("/", 10), Parenthesis(
			"(", Integer.MIN_VALUE), Factorial("!", 10);
	private final String textRepresentation;
	private final int precedence;

	Operator(String textRepresentation, int precedence) {
		this.textRepresentation = textRepresentation;
		this.precedence = precedence;
	}

	public static Operator fromString(String s) {
		for (Operator o : Operator.values()) {
			if (s.equals(o.textRepresentation))
				return o;
		}
		throw new IllegalArgumentException("No operator found for " + s);
	}

	public String getTextForm() {
		return textRepresentation;
	}

	public int getPrecedence() {
		return precedence;
	}
}